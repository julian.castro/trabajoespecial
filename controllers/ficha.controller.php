<?php
require_once 'proyecto.controler.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.

class ControladorFicha extends ControladorPadre {

    
    public function subirFicha() {
        $this->VerificarRegistro();

        $titulo = $_GET['titulo'];
        $descripcion = $_GET['descripcion'];
        $so = $_GET['so'];
        $graficos = $_GET['grafico'];
        $espacio =$_GET['espacio'];
        $link = $_GET['link'];
        $imagen = $_GET['imagen'];
        $categoria = $_GET['categoria'];

        $requisitos= $this->requisitos($so, $graficos, $espacio);
        // inserta en la DB y redirige
        $this->traerModeloFichas()-> insertarFicha($titulo, $descripcion, $categoria, $requisitos, $imagen, $link);
        header("Location: " . BASE_URL . "");

    }

//router20/fichacontroller/33/fichamodel43-----------------------------------------------------------------------------------
//router20/fichacontroller/33/fichaview14-----------------------------------------------------------------------------------

    public function listarFichas() {
        // pido todos los productos al MODELO
        $fichas = $this->traerModeloFichas()->traerTodasFichas();
        // actualizo la vista
         $this->traerVistaFicha()->mostrarGaleria($fichas);
    }
//router26/fichacontroller40/
    public function listadoJuegos( $juego){
        if ($juego=="todos"){
            $fichas = $this->traerModeloFichas()->traerTodasFichas();
            $this->traerVistaFicha()->tablaJuegos($fichas);
        }
    }

//-----------------------------------------------------------------------------------

    public function mostrarJuego($juego){
        $ficha=$this->traerModeloFichas()->traerFicha($juego);
        $this->traerVistaFicha()->mostrarFicha($ficha);
    }
    private function requisitos ($so, $graficos, $espacio){
        $requisito= $so . '/' . $graficos . '/' .$espacio ;

        return $requisito;

    }
    public function eliminarJuego($id_juego) {
        $this->VerificarRegistro();
        $this->traerModeloFichas()->eliminarJuego($id_juego);
        header("Location: " . BASE_URL . "listadojuegos/todos");
    }
         
//-----------------------------------------------------------------------------------

    public function confirmarcambiosjuegos($ficha){
        $this->VerificarRegistro();
        $titulo = $_GET['titulo'];
        $descripcion = $_GET['descripcion'];
        $so = $_GET['so'];
        $graficos = $_GET['grafico'];
        $espacio =$_GET['espacio'];
        $link = $_GET['link'];
        $imagen = $_GET['imagen'];
        $categoria = $_GET['categoria'];

        $requisitos= $this->requisitos($so, $graficos, $espacio);
        $this->traerModeloFichas()-> modificarJuego($titulo, $descripcion, $categoria, $requisitos, $imagen, $link, $ficha);
        header("Location: " . BASE_URL . "listadojuegos/todos");
    }
}

