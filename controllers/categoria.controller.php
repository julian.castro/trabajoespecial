<?php
require_once 'proyecto.controler.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.

class ControladorCategoria extends ControladorPadre {

    
    
    public function subirCategoria(){
        $this->VerificarRegistro();
        $categoria = $_GET['categoria'];
        $this->traerModeloFichas()-> insertarCategoria($categoria);
        header("Location: " . BASE_URL . "listadoCategorias/todos");
    }

//----------------------------------------------------------------------
    
    public function listadoCategorias( $opcion){
        if ($opcion=="todos"){
            $this->VerificarRegistro();
            $this->traerVistaFicha()->tablaCategorias();

        }else{
            $juegos = $this->traerModeloFichas()->traerFichasPorCategoria($opcion);
            $this->traerVistaFicha()->tablaJuegosPorCategoria($juegos);
        }
           
    }

//-----------------------------------------------------------------------------------
    //router94/categoriacontroller34/categoriamodelo30
    public function eliminarCategoria($id_categoria) {
        $this->VerificarRegistro();
        $this->traerModeloCategoria()->eliminarCategoria($id_categoria);
        header("Location: " . BASE_URL . "listadoCategorias/todos");
    }
//-----------------------------------------------------------------------------------
    public function confirmarCambiosCategoria($categoria){
        $this->VerificarRegistro();
        $titulo = $_GET['categoria'];

        $this->traerModeloCategoria()-> modificarCategoria($titulo, $categoria);
        header("Location: " . BASE_URL . "listadoCategorias/todos");
    }
    public function error(){
        $this->traerVistaFicha()->error();
    }
}

