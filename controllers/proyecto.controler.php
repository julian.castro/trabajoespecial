<?php
require_once 'models/usuario.model.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.
require_once 'views/formulario.view.php';//enlazo con archivo de vista.

class ControladorPadre {

    private $modeloUsuario;
    private $modeloCategoria;
    private $modeloFichas;//creo 2 variables de donde iniciar las clases.
    private $vistaFicha;
    private $vistaFormulario;

    public function __construct() {//cuando se llama a la clase el CONSTRUCT realiza una accion por default.
        $this->modeloCategoria = new ModeloCategoria();
        $this->modeloUsuario = new ModeloUsuario();
        $c=$this->modeloCategoria->traerCategorias();
        $usuario=$this->permiso();
        $this->modeloFichas = new ModeloFicha();//inicio las clases dentro de las variables.
        $this->vistaFicha = new VistaFicha($c, $usuario);
        $this->vistaFormulario = new FormularioVista($c, $usuario);

    }
    private function permiso() {
        session_start(); 
        $nombre= 'no';
        if (!isset($_SESSION['registrado'])) {
            $nombre= 'no';
        }else{
            $nombre=$_SESSION['email'];
            
        }
        return $nombre;
    }
    static private function start() {
        if (session_status() != PHP_SESSION_ACTIVE)
            session_start();
    }
    static public function VerificarRegistro() {
        // llama al metodo estatico privado para iniciar la session
        self::start(); 

        if (!isset($_SESSION['registrado'])) {
            header('Location: ' . BASE_URL . 'ingresar');
            die();
        }
    }

    public function traerModeloCategoria(){
         return $this->modeloCategoria;
    }
    public function traerModeloFichas(){
         return $this->modeloFichas;
    }
    public function traerVistaFicha(){
         return $this->vistaFicha;
    }
    public function traerVistaformulario(){
         return $this->vistaFormulario;
    }
    public function traerModeloUsuario(){
         return $this->modeloUsuario;
    }
}