<?php

require_once 'models/usuario.model.php';
require_once 'proyecto.controler.php';
require_once 'models/categoria.model.php';
require_once 'models/ficha.model.php';//enlazo con archivo de modelo.
require_once 'views/ficha.view.php';//enlazo con archivo de vista.

class ControladorAutentificador extends ControladorPadre {

    
    public function verificar() {
        $email = $_POST['email'];
        $contraseña = $_POST['contraseña'];

        // busco el usuario
        $usuario = $this->traerModeloUsuario()->traerUsuario($email);

        // si existe el usuario y el password es correcto
        if ($usuario && password_verify($contraseña, $usuario->contraseña)) { 

            // abro session y guardo al usuario logueado
            session_start();
            $_SESSION['registrado'] = true;
            $_SESSION['id_usuario'] = $usuario->id_usuario;
            $_SESSION['email'] = $usuario->email;
            
            header("Location: " . BASE_URL . "");
        } else {
            header("Location: " . BASE_URL . "ingresar");
        }

    }
    /**
     * Destruye la session del usuario
     */
    public function cerrarSesion() {
        session_start();
        session_destroy();
        header("Location: " . BASE_URL . '');
    }





}