<?php

class ControladorFormulario extends ControladorPadre {


    public function formularioCargarJuego(){
        $this->VerificarRegistro();
        $this->traerVistaFormulario()->imprimirFormularioCargarJuego();
    }
    public function formularioModificarJuego($id_juego){
        $this->VerificarRegistro();
        $ficha=$this->traerModeloFichas()->traerFicha($id_juego);
        $this->traerVistaFormulario()->imprimirFormularioModificarJuego($ficha);
    }

    public function formularioIngresar(){
        $this->traerVistaFormulario()->imprimirFormularioIngresar();
    }

    public function formularioCargarCategoria(){
        $this->VerificarRegistro();
        $this->traerVistaFormulario()->imprimirFormularioCargarCategoria();    
    }

    public function formularioModificarCategoria($id_categoria){
        $this->VerificarRegistro();
        $categoria=$this->traerModeloCategoria()->traerCategoria($id_categoria);
        $this->traerVistaFormulario()->imprimirFormularioModificarCategoria($categoria);    
    }
    function error(){
        $this->traerVistaFormulario()->error();
    }

}