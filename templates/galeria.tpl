{include 'header.tpl'}
{include 'barradenavegacion.tpl'}


<div class="row align-items-stretch contenedorficha ">
  {foreach $juegos item=juego }
    <div class="col-sm-2 fichainicio ">
        
      <div class="card ">
           
        <div class="ficha">
          <img src="{$juego->img}" class="card-img" alt="">   
          <div class="contenidoficha">
            <h5 class="card-title">{$juego->titulo}</h5>
            <p class="card-text">Categoría: {$juego->categoria_titulo}</p>
            <a href="fichajuegos/{$juego->id_ficha}" class="btn btn-primary">Ver mas</a>
          </div>  
        </div>
      </div>
    </div>
  {/foreach}
</div>

{include 'piedepagina.tpl'}

