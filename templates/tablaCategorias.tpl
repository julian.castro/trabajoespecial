{include 'header.tpl'}
{include 'barradenavegacion.tpl'}


<div class="row contenedorficha ">
  <table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">Categoría</th>
        <th scope="col">Eliminar</th>
        <th scope="col">Modificar</th>
      </tr>
    </thead>
    {foreach $categorias item=categoria }
      <tbody>
        <tr>
          <th scope="row">
            <a href="filtrar/{$categoria->id_categoria}" class="">{$categoria->titulo}</a>
          </th>
          <td>
            <a type="button" href="eliminarcategoria/{$categoria->id_categoria}" class="btn btn-danger">Eliminar</a>
          </td>
          <td> 
            <a type="button" href="modificarcategoria/{$categoria->id_categoria}" class="btn btn-primary">Modificar</a>
          </td>
        </tr>
      </tbody>
    {/foreach}
  </table>
</div>


{include 'piedepagina.tpl'}