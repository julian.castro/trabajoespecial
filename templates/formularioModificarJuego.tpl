<div class="contenedorform">

  {include 'header.tpl'}
  {include 'barradenavegacion.tpl'}
  <div class="ingresarjuego">
    <form action="confirmarcambiosjuegos/{$juego->id_ficha}" method="GET">
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault01" class="textoform">Titulo</label>
          <input type="text" class="form-control" id="validationDefault01" name="titulo" value="{$juego->titulo}">
        </div>
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Imagen del Juego</label>
          <input type="text" class="form-control" id="validationDefault03"  name="imagen" value="{$juego->img}">
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-12 mb-3">
          <label for="validationDefault03" class="textoform">Descripcion del juego</label>
          <input type="text" class="form-control" id="validationDefault03"  name="descripcion"  value="{$juego->descripcion}">
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Sistema Operativo</label>
          <input type="text" class="form-control" id="validationDefault03"  name="so" value="{$juego->so}">
        </div>
    
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Video</label>
          <input type="text" class="form-control" id="validationDefault03"  name="grafico" value="{$juego->graficos}">
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault03" class="textoform">Espacio en disco</label>
          <input type="text" class="form-control" id="validationDefault03"  name="espacio" value="{$juego->espacio}">
        </div>

         
        <div class="col-md-4 mb-3">
          <label for="validationDefault04" class="textoform">Categoria</label>
            <select class="custom-select" name= 'categoria' required>
              <option selected disabled value="" >Elegir...</option>
                {foreach $categorias item=categoria }
                    <option value="{$categoria->id_categoria}"> {$categoria->titulo} </option>
                {/foreach}
            </select>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-12 mb-3">
          <label for="validationDefault02" class="textoform">Link de descarga</label>
          <input type="text" class="form-control" id="validationDefault02" name="link" value="{$juego->link}">
        </div>
      </div>
      <button class="btn btn-primary" type="submit">Confirmar cambios</button>
    </form>
  </div>

</div>