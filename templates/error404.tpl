{include 'header.tpl'}
{include 'barradenavegacion.tpl'}


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h1>
                    Oops!</h1>
                <h2>
                    404 NOT FOUND</h2>
                <div class="error-details">
                    ¡¡Disculpe, a ocurrido un error, esta pagina no responde!!
                </div>
                <div class="error-actions">
                    <a href="" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                        INICIO</a>
                    </div>
            </div>
        </div>
    </div>
</div>

{include 'piedepagina.tpl'}

