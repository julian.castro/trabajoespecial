{include 'header.tpl'}
{include 'barradenavegacion.tpl'}

<div class="contenedorjuego">
  <div class="card juego ">
    <img src="{$juego->img}" class="card-img-top" alt="{$juego->titulo}">
    <div class="card-body">
      <h5 class="card-title ">{$juego->titulo}</h5>
      <p class="card-text">{$juego->descripcion}</p>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item">Categoria : {$juego->categoria_titulo} </li> 
      <li class="list-group-item">Placa de video: {$juego->so} </li>
      <li class="list-group-item">sistema operativo: {$juego->graficos}</li>
      <li class="list-group-item">Espacio en disco: {$juego->espacio}</li>
    </ul>
    <div class="card-body">
      <a href="{$juego->link}" class="card-link">Descargar</a>
    </div>
  </div>
</div>

{include 'piedepagina.tpl'}