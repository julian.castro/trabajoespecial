<nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="home"><h1>Todo juegos</h1></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="home">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Categorias
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                {foreach $categorias item=categoria }
                  <a class="dropdown-item" href="listadoCategorias/{$categoria->id_categoria}"> {$categoria->titulo} </a> 
                {/foreach}
                
              </div>
            </li>
          </ul>
          {if $usuario=='no'}
            <div class="form-inline my-2 my-lg-0">
              <a href="ingresar" class="btn btn-outline-primary my-2 my-sm-0 ">ingresar</a>
              
            </div>
            {else}
              <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {$usuario}
                </button>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="cargarjuego">Cargar Juego</a>
                  <a class="dropdown-item" href="listadojuegos/todos">Modificar Juego</a>
                  <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="cargarcategoria">Cargar Categoria</a>
                    <a class="dropdown-item" href="listadoCategorias/todos">Modificar Categoria</a>
                  <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="cerrarsesion"><span>Cerrar sesion</span></a>
                </div>
          {/if}
          
        </div>
        
      </nav>
    