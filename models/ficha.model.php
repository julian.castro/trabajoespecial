<?php
require_once "controllers/formulario.controller.php";
class ModeloFicha{

//--------------------------------------------------------------------------

    //creo una funcion q establece la coneccion.
    private function crearConexion() {
        //listo en variables los datos para abrir la coneccion.
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
        $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            $error=new ControladorFormulario;
            $error->error();
        }
        return $pdo;
    }

//--------------------------------------------------------------------------

    public function traerFicha($juego){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("SELECT ficha.*, categorias.titulo as categoria_titulo FROM ficha, categorias WHERE id_ficha = ?");
        $sentencia->execute([$juego]);
        $ficha = $sentencia->fetch(PDO::FETCH_OBJ);
        $requisitos= explode('/',$ficha->requisitos);
        $ficha->so=$requisitos[0];
        $ficha->graficos=$requisitos[1];
        $ficha->espacio=$requisitos[2];
         return $ficha;      

    }

//router20/fichacontroller33/fichamodel43---------------------------------------------------------------------
//router26/fichacontroller40/fichamodel43----------------------------------------------------------------------
    public function traerTodasFichas() {    //traigo todos los juegos de la base de datos.

        $db = $this->crearConexion();//llamo a la funcion para abrir la coneccion.
        $sql="SELECT ficha.*, categorias.titulo as categoria_titulo FROM ficha, categorias WHERE ficha.id_categoria=categorias.id_categoria";
        $query = $db->prepare($sql);//preparo el pedido.
        $query->execute();//ejecuto el pedido.

        $fichas = $query->fetchAll(PDO::FETCH_OBJ);//capturo el pedido y lo guardo.
       
        return $fichas;//retorno el pedido.
        
    }

//---------------------------------------------------------------------
    
    public function insertarFicha($titulo, $descripcion, $categoria, $requisitos, $imagen, $link) {
        // 1. abro la conexión con MySQL 
        $db = $this->crearConexion();

        // 2. enviamos la consulta
        $sentencia = $db->prepare("INSERT INTO ficha(titulo, descripcion, requisitos, img, link, id_categoria)
         VALUES(?, ?, ?, ?, ?, ?)"); // prepara la consulta
        $sentencia->execute([$titulo, $descripcion, $requisitos, $imagen, $link, $categoria]); // ejecuta
    }

//--------------------------------------------------------------------------

    public function modificarJuego($titulo, $descripcion, $categoria, $requisitos, $imagen, $link, $ficha) {
        // 1. abro la conexión con MySQL 
        $db = $this->crearConexion();
        // 2. enviamos la consulta
        $sentencia = $db->prepare("UPDATE ficha SET titulo = ? , descripcion = ? , 
        requisitos = ? , img = ? , link = ?, id_categoria = ? WHERE ficha.id_ficha = ? "); // prepara la consulta
        $sentencia->execute([$titulo, $descripcion, $requisitos, $imagen, $link, $categoria, $ficha]); // ejecuta
    }

//----------------------------------------------------------------------------

    public function eliminarJuego($id_juego){
        $db = $this->crearConexion();
        // 2. enviamos la consulta
        $sentencia = $db->prepare("DELETE FROM ficha WHERE id_ficha = ?"); // prepara la consulta
        $sentencia->execute([$id_juego]); // ejecuta    
    }

  //----------------------------------------------------------------------------
  
    public function insertarCategoria($categoria){
        // 1. abro la conexión con MySQL 
        $db = $this->crearConexion();

        // 2. enviamos la consulta
        $sentencia = $db->prepare("INSERT INTO categorias(titulo) VALUES(?)"); // prepara la consulta
        $sentencia->execute([$categoria]); // ejecuta
    }

//----------------------------------------------------------------------------

    public function traerFichasPorCategoria($id_categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("SELECT * FROM ficha WHERE ficha.id_categoria= ?");
        $sentencia->execute([$id_categoria]);
        return $sentencia->fetchAll(PDO::FETCH_OBJ);
    }

}
