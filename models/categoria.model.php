<?php
class ModeloCategoria{
    //creo una funcion q establece la coneccion.
    private function crearConexion() {
        //listo en variables los datos para abrir la coneccion.
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
        $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $pdo;
    }
    function traerCategorias() {//traigo todas las  categorias.

        $db = $this->crearConexion();
        
        $query = $db->prepare("SELECT * FROM categorias");
        $query->execute();

        $categorias = $query->fetchAll(PDO::FETCH_OBJ);

        return $categorias;
    } 
//-------------------------------------------------------------------------------------------------------------
    //router94/categoriacontroller34/categoriamodelo30
    public function  eliminarCategoria($id_categoria){
        $db = $this->crearConexion();
        // 2. enviamos la consulta
        $sentencia = $db->prepare("DELETE FROM categorias WHERE id_categoria = ?"); // prepara la consulta
        $sentencia->execute([$id_categoria]); // ejecuta    
    }
    public function traerCategoria($id_categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("SELECT * FROM categorias WHERE id_categoria = ?");
        $sentencia->execute([$id_categoria]);
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }
    public function  modificarCategoria($titulo, $categoria){
        $db = $this->crearConexion();
        $sentencia = $db->prepare("UPDATE categorias SET titulo = ? WHERE categorias.id_categoria= ? "); 
        $sentencia->execute([$titulo, $categoria]);
    }
    

}