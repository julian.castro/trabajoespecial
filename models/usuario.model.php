<?php

class ModeloUsuario {

    private $db;

    public function __construct() {
        $this->db = $this->crearConexion();
    }

     /**
     * Crea la conexion
     */
    private function crearConexion() {
        //listo en variables los datos para abrir la coneccion.
        $host = 'localhost';
        $nombreDeUsuario = 'root';
        $contraseña = '';
        $baseDeDatos = 'db_fichas';
        try {
        $pdo = new PDO("mysql:host=$host;dbname=$baseDeDatos;charset=utf8", $nombreDeUsuario, $contraseña);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $pdo;
    }

    public function traerUsuario($email) {
        $sentencia = $this->db->prepare("SELECT * FROM usuarios WHERE email = ?");
        $sentencia->execute([$email]);
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }

}