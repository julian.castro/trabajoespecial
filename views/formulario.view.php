<?php
require_once('libs/Smarty.class.php');
require_once('constructor.view.php');

class FormularioVista extends ConstructorVista{

    public function imprimirformularioCargarJuego(){
        $this->traerSmarty()->display('formularioJuego.tpl');         
    }

//******************************************************************************************* */

    public function imprimirformularioModificarJuego($ficha){
        $this->traerSmarty()->assign('juego', $ficha);
        $this->traerSmarty()->display('formularioModificarJuego.tpl');         
    }

//******************************************************************************************* */
   
    public function imprimirFormularioCargarCategoria(){
        $this->traerSmarty()->display('agregarcategoria.tpl');         
    }

//***************************************************************************************** */
    
    public function mostrarFicha($ficha){
        $this->traerSmarty()->assign('juego', $ficha);
        $this->traerSmarty()->display('fichajuego.tpl');
    }

//***************************************************************************************** */
    
    public function imprimirFormularioIngresar(){
        $this->traerSmarty()->display('formularioIngresar.tpl');
    }

//**************************************************************************************** */

    public function imprimirFormularioModificarCategoria($categoria){
        $this->traerSmarty()->assign('categoria', $categoria);
        $this->traerSmarty()->display('formularioModificarCategoria.tpl'); 
    }

//**************************************************************************************** */

    public function error(){
        $this->traerSmarty()->display('error404.tpl'); 
    }

}


  
