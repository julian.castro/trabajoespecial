<?php
require_once('libs/Smarty.class.php');
require_once('constructor.view.php');

class VistaFicha extends ConstructorVista{

    public function mostrarFicha($ficha){
        $this->traerSmarty()->assign('juego', $ficha);
        $this->traerSmarty()->display('fichajuego.tpl');
    }
//router20/fichacontroller/33/fichaview14-----------------------------------------------------------------------------------
    public function mostrarGaleria($fichas){
        $this->traerSmarty()->assign('juegos', $fichas);
        $this->traerSmarty()->display('galeria.tpl');
    }
    public function tablaJuegos($fichas){
        $this->traerSmarty()->assign('fichas', $fichas);
        $this->traerSmarty()->display('tablaJuegos.tpl');
    }
    public function tablaCategorias(){
        $this->traerSmarty()->display('tablaCategorias.tpl');
    }
    public function tablaJuegosPorCategoria($ficha){
        $this->traerSmarty()->assign('fichas', $ficha);
        $this->traerSmarty()->display('tabla.tpl'); 
    }
    public function error(){
        $this->traerSmarty()->display('error404.tpl'); 
    }

}


  
