<?php
require_once('libs/Smarty.class.php');

class ConstructorVista{
    private $smarty;
    public function __construct($categorias, $usuario){
        $this->smarty = new Smarty();
        $this->smarty->assign('categorias', $categorias);
        $this->smarty->assign('usuario', $usuario);
        $this->smarty->assign('base_url', BASE_URL);
    }
    public function traerSmarty(){
        return $this->smarty;
    }
}


  
