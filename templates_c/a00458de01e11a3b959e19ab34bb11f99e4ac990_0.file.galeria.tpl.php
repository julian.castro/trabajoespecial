<?php
/* Smarty version 3.1.34-dev-7, created on 2020-05-28 19:10:57
  from 'C:\xampp\htdocs\trabajoespecial\templates\galeria.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ecff0a1aa3e72_63367897',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a00458de01e11a3b959e19ab34bb11f99e4ac990' => 
    array (
      0 => 'C:\\xampp\\htdocs\\trabajoespecial\\templates\\galeria.tpl',
      1 => 1590671284,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
    'file:piedepagina.tpl' => 1,
  ),
),false)) {
function content_5ecff0a1aa3e72_63367897 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="row align-items-stretch contenedorficha ">
  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['juegos']->value, 'juego');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['juego']->value) {
?>
    <div class="col-sm-2 fichainicio ">
        
      <div class="card ">
           
        <div class="ficha">
          <img src="<?php echo $_smarty_tpl->tpl_vars['juego']->value->img;?>
" class="card-img" alt="">   
          <div class="contenidoficha">
            <h5 class="card-title"><?php echo $_smarty_tpl->tpl_vars['juego']->value->titulo;?>
</h5>
            <p class="card-text">Categoría: <?php echo $_smarty_tpl->tpl_vars['juego']->value->categoria_titulo;?>
</p>
            <a href="fichajuegos/<?php echo $_smarty_tpl->tpl_vars['juego']->value->id_ficha;?>
" class="btn btn-primary">Ver mas</a>
          </div>  
        </div>
      </div>
    </div>
  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>

<?php $_smarty_tpl->_subTemplateRender('file:piedepagina.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
