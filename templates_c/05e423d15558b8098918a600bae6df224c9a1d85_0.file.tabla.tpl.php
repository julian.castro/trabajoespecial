<?php
/* Smarty version 3.1.34-dev-7, created on 2020-05-28 19:10:52
  from 'C:\xampp\htdocs\trabajoespecial\templates\tabla.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5ecff09c241e99_13281951',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '05e423d15558b8098918a600bae6df224c9a1d85' => 
    array (
      0 => 'C:\\xampp\\htdocs\\trabajoespecial\\templates\\tabla.tpl',
      1 => 1590631947,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:barradenavegacion.tpl' => 1,
    'file:piedepagina.tpl' => 1,
  ),
),false)) {
function content_5ecff09c241e99_13281951 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:barradenavegacion.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="row contenedorficha ">
  <table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">Categoría</th>
        <th scope="col">Eliminar</th>
        <th scope="col">Modificar</th>
      </tr>
    </thead>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fichas']->value, 'ficha');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ficha']->value) {
?>
      <tbody>
        <tr>
          <th scope="row">
            <a href="filtrar/<?php echo $_smarty_tpl->tpl_vars['ficha']->value->id_ficha;?>
" class=""><?php echo $_smarty_tpl->tpl_vars['ficha']->value->titulo;?>
</a>
          </th>
          <td>
            <a type="button" href="eliminarcategoria/<?php echo $_smarty_tpl->tpl_vars['ficha']->value->id_ficha;?>
" class="btn btn-danger">Eliminar</a>
          </td>
          <td> 
            <a type="button" href="modificarcategoria/<?php echo $_smarty_tpl->tpl_vars['ficha']->value->id_ficha;?>
" class="btn btn-primary">Modificar</a>
          </td>
        </tr>
      </tbody>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  </table>
</div>


<?php $_smarty_tpl->_subTemplateRender('file:piedepagina.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
